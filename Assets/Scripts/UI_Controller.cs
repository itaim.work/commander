using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Controller : MonoBehaviour
{
    bool asdads = true;
    [SerializeField] private Dropdown Move_unit_Selection_Menu;
    [SerializeField] private Dropdown Move_Node_Selection_Menu;
    [SerializeField] private Dropdown Attack_unit_Selection_Menu;
    [SerializeField] private Dropdown Attack_Node_Selection_Menu;

    [SerializeField] private GameObject MoveMenu, PauseMenu, LoseMenu, WinMenu;
    [SerializeField] private GameObject AttackMenu;
    [SerializeField] private GameObject ChooseMenu;

    [SerializeField] private bool MoveActive;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private Text TurnDisplay;
    public List<GameObject> temp;

    // Start is called before the first frame update
    void Start()
    {
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        TurnDisplay = GameObject.Find("Turn_Display").GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        Move_Node_Selection_Menu.onValueChanged.AddListener(delegate
        {
            GameManagerScript.UpdateActive();

        });


        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseGame();
        }

        Move_unit_Selection_Menu.onValueChanged.AddListener(delegate
        {
            DropdownValueChanged();
        });
    }

    void PopulateDropdown(Dropdown dropdown, List<GameObject> optionsArray)
    {
        List<string> options = new List<string>();
        foreach (var option in optionsArray)
        {
            if (option.CompareTag("Unit"))
            {
                options.Add(option.GetComponent<Unit>().GetName() + " Strengh: " + option.GetComponent<Unit>().calcStrengh());
            }
            else if (option.CompareTag("Node"))
            {
                if (option.GetComponent<Node_Script>().GetIsOccupied())
                {
                    options.Add(option.GetComponent<Node_Script>().GetName()); // Or whatever you want for a label

                }
                else
                {
                    options.Add(option.GetComponent<Node_Script>().GetName()); // Or whatever you want for a label
                }
            }
        }
        dropdown.ClearOptions();
        dropdown.AddOptions(options);
    }
    public string GetTerritoryDropDownMenuValue()
    {
        if (MoveActive)
        {
            return Move_Node_Selection_Menu.options[Move_Node_Selection_Menu.value].text;
        }
        return Attack_Node_Selection_Menu.options[Attack_Node_Selection_Menu.value].text;
    }
    public void OpenAttack()
    {
        MoveActive = false;
        List<GameObject> dropdownObjects = GameManagerScript.getUnits();
        List<GameObject> temp = new List<GameObject>();

        if (GameManagerScript.GetPlayer_1_Turn())
        {
            foreach (var GameObject in dropdownObjects)
            {
                if (GameObject.GetComponent<Unit>().Get_Player_1())
                {
                    temp.Add(GameObject);
                }
            }
        }
        else
        {
            foreach (var GameObject in dropdownObjects)
            {
                if (!GameObject.GetComponent<Unit>().Get_Player_1())
                {
                    temp.Add(GameObject);
                }
            }
        }
        PopulateDropdown(Attack_unit_Selection_Menu, temp);

        GameManagerScript.UpdateActiveUnit();
        temp = GameManagerScript.getActiveUnit().GetActiveNode().GetNodesInRangeList();

        temp = ClearSelfNode(temp);
        PopulateDropdown(Attack_Node_Selection_Menu, temp);

        ChooseMenu.SetActive(false);
        AttackMenu.SetActive(true);
        GameManagerScript.UpdateActiveNode();
    }
    public void OpenMove()
    {
        MoveActive = true;
        List<GameObject> dropdownObjects = GameManagerScript.getUnits();
        temp = new List<GameObject>();
        if (GameManagerScript.GetPlayer_1_Turn())
        {
            foreach (var gameTemp in dropdownObjects)
            {
                if (gameTemp.GetComponent<Unit>().Get_Player_1())
                {
                    temp.Add(gameTemp);
                }
            }
        }
        else
        {

            foreach (var gameTemp in dropdownObjects)
            {
                if (!gameTemp.GetComponent<Unit>().Get_Player_1())
                {
                    temp.Add(gameTemp);
                }
            }

        }
        PopulateDropdown(Move_unit_Selection_Menu, temp);
        GameManagerScript.UpdateActiveUnit();
        temp = GameManagerScript.getActiveUnit().GetActiveNode().GetNodesInRangeList();
        temp = ClearOccupiedNode(temp);
        PopulateDropdown(Move_Node_Selection_Menu, temp);
        ChooseMenu.SetActive(false);
        MoveMenu.SetActive(true);
        GameManagerScript.UpdateActiveNode();
    }
    public List<GameObject> ClearSelfNode(List<GameObject> temp)
    {
        List<GameObject> EnemyList = new List<GameObject>();
        if (GameManagerScript.GetPlayer_1_Turn())
        {
            foreach (var option in temp)
            {
                if (!option.GetComponent<Node_Script>().GetIsPlayer())
                {
                    EnemyList.Add(option);
                }
            }
        }
        else
        {
            foreach (var option in temp)
            {
                if (option.GetComponent<Node_Script>().GetIsPlayer())
                {
                    EnemyList.Add(option);
                }
            }

        }


        return EnemyList;
    }
    public List<GameObject> ClearOccupiedNode(List<GameObject> temp)
    {
        List<GameObject> cleanList = new List<GameObject>();

        foreach (var option in temp)
        {
            if (!option.GetComponent<Node_Script>().GetIsOccupied())
            {
                cleanList.Add(option);
            }
        }


        return cleanList;
    }
    public void DropdownValueChanged()
    {
        List<GameObject> temp = GameManagerScript.getUnits()[Move_unit_Selection_Menu.value].GetComponent<Unit>().GetActiveNode().GetNodesInRangeList();
        PopulateDropdown(Move_Node_Selection_Menu, temp);

    }
    public string GetDropDownString()
    {
        if (MoveActive)
        {
            print(Move_unit_Selection_Menu.options[Move_unit_Selection_Menu.value].text);
            return Move_unit_Selection_Menu.options[Move_unit_Selection_Menu.value].text;
        }
        return Attack_unit_Selection_Menu.options[Attack_unit_Selection_Menu.value].text;
    }
    public bool GetMoveActive()
    {
        return MoveActive;
    }
    public void SetTurnDisplayText(String text)
    {
        TurnDisplay.text = text;
    }
    public void Reset_UI()
    {
        AttackMenu.SetActive(false);
        MoveMenu.SetActive(false);
        ChooseMenu.SetActive(true);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
    }

    public void WinGame()
    {
        Time.timeScale = 0;
        WinMenu.SetActive(true);
    }
    public void LoseGame()
    {
        Time.timeScale = 0;
        LoseMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
