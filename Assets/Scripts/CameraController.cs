using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float KeyHorizontal;
    [SerializeField] private float KeyVertical;
    [SerializeField] private float Speed;


    // Update is called once per frame
    void Update()
    {
        KeyHorizontal = Input.GetAxis("Horizontal");
        KeyVertical = Input.GetAxis("Vertical");

        if (transform.position.x >= 20) // most positive x pos value
        {
            KeyHorizontal = Mathf.Clamp(KeyHorizontal, -1f, 0f); // allow only negetive movement
        }
        if (transform.position.x <= -20) // most negetive x pos value
        {
            KeyHorizontal = Mathf.Clamp(KeyHorizontal, 0f, 1f); // allow only positive movement
        }
        if (transform.position.z <= -20) // most negetive z pos value
        {
            KeyVertical = Mathf.Clamp(KeyVertical, 0f, 1f); // allow only positive movement
        }

        if (transform.position.z >= 20) // most positive z pos value
        {
            KeyVertical = Mathf.Clamp(KeyVertical, -1f, 0f); // allow only negetive movement
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {

        }

        transform.position += new Vector3(KeyHorizontal, -Input.GetAxis("Mouse ScrollWheel") * 100, KeyVertical) * Time.deltaTime * Speed;
    }

}
