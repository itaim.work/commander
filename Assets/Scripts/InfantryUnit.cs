using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfantryUnit : Unit
{

    [SerializeField] private int DamageBuffer;


    // Start is called before the first frame update
    void Start()
    {
        DamageBuffer = this.UnitCounter * 2;
        SpawnUnits();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override int calcStrengh()
    {
        return this.UnitStrengh * StrenghMultyplier + DamageBuffer;
    }

    public override void TakeDamage(int damage)
    {
        this.UnitCounter -= damage;
    }

    public override void AddUnits(int amount)
    {
        throw new System.NotImplementedException();
    }

}
